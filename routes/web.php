<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Route::get('/test', function () {
    //     return view('admin.template.master');
    // });
    
    
// Route::get('/home', 'HomeController@index')->name('home');

Route::get('/', function () {
return view('welcome');
});


// Route::get('/user/book', function () {
    // return view('user.layouts.book');
    // });
    // Route::get('/admin/listusers', function(){
        //     return view('admin.layout.component.listusers.table');
        // });

        
        Auth::routes();
        
        // Route::get('/admin', function (){
//     return view('admin.layout.home');
// })->middleware('auth');



Route::get('/excel-user', 'Admin\ListuserController@export');
Route::get('/excel-book', 'Admin\BookController@export');
Route::resource('create-profile', 'User\ProfileController')->middleware('auth');

Route::get('/admin', 'Admin\AdminController@index');
Route::resource('admin/books', 'Admin\BookController')->middleware('auth');
Route::resource('admin/listusers', 'Admin\ListuserController')->middleware('auth');

Route::get('/user', 'User\UserController@home');
Route::resource('user/book', 'User\UserController')->middleware('auth');
Route::get('/user/history', 'User\UserController@history')->middleware('auth');


