## Informasi Kelompok

Nomor Kelompok : 15

Anggota :
- Yogi Mahaputra (@Yogimahaputra)
- Bintang Muhammad Gibran (@Bintang Gibran)
- Al-Faris Azmardiansyah Wijaya (@alfaris212)

Tema Project : 

Website Peminjaman Buku 

## Link Video Demo

(https://drive.google.com/file/d/12QWK49piNUct9Otwp05jPR_4WWrY5Kof/view?usp=sharing)

<a><img src="{{ asset('/assets/img/erd-peminjaman-buku.jpg') }} ">
</a>
## Link Deploy Hosting

[Belum Tersedia]
