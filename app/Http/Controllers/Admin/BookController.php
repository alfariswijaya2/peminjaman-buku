<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Exports\BooksExport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;
use App\Books;
use App\genres;
use DB;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $book = Books::all();
    	return view('admin.layout.component.books.table',['books' => $book]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.layout.component.books.form');
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required|unique:books',
            'author' => 'required',
            'publish' =>'required',
        ]);
        

        // dd($request->all());
        
        if($request["genre"] !== null) {
            $tmp = strtolower($request["genre"]);
            $genre_arr = explode(',', $tmp);
            
            $genre_id = [];
            foreach ($genre_arr as $genre_name) {
                $genre = genres::firstOrCreate(['name' => $genre_name]);
                
                $genre_id = $genre->id;
            }
        }
        
        $title = strtolower($request["title"]);
        
        $books = DB::table('books')->insert(
            [
                'title' => $title,
                'author' => $request['author'],
                'publish' =>  $request['publish'],
            ]
        );


        return redirect('/admin/books');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // $book = Books::find($id);
        // $book->delete();

        $book = DB::table('books')->where('id',$id)->delete();
        return redirect()->back();
    }

    public function export() 
    {
        return Excel::download(new BooksExport, 'books.xlsx');
    }
}
