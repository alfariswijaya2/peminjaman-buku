<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Borrow;
use App\Books;
use App\profiles;
use DB;
use Auth;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $auth = Auth::id();
        $book = Books::all();
        $profiles = DB::table('profiles')->where('user_id', $auth)->first();
        return view('user.layouts.book',compact('book','profiles'));
    }
    public function home()
    {
        $book = Books::all();
        return view('user.layouts.home',compact('book'));
    }
    public function history()
    {
        $auth = Auth::id();
        $profiles = DB::table('profiles')->where('user_id', $auth)->first();
        $join = DB::table('borrows')
        ->join('books', 'book_id', '=', 'books.id')
        ->join('users', 'user_id', '=', 'users.id')
        ->where('user_id', $auth)
        ->get();
        return view('user.layouts.history',compact('join','profiles'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $book_nama = strtolower($request["book_id"]);
        $book = DB::table('books')->where('title', $book_nama)->first();

        if($book === null){
            return redirect('/user/book')->with('danger','Buku yang dipinjam tidak tersedia');
        }

        $user = strtolower($request["user_id"]);
        $user = DB::table('users')->where('username', $user)->first();


        $request->validate([
            'borrow_date' => 'required',
            'book_return' => 'required',
            'user_id' =>'required',
            'book_id' =>'required',
        ]);

        Borrow::create([
            'borrow_date' => $request['borrow_date'],
            'book_return' => $request['book_return'],
            'user_id' => $user->id,
            'book_id' => $book->id,
    	]);

        return redirect('/user/history')->with('success',' Berhasil Ditambah');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
