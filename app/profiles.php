<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class profiles extends Model
{
    protected $table = "profiles";
    protected $fillable = ['name','age','address','user_id'];
}
