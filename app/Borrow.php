<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Borrow extends Model
{
    protected $table = "borrows";
    protected $fillable = ['borrow_date','book_return','user_id', "book_id"];
}
