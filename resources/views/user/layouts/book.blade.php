@extends('user.template.master')

@section('content')
<style>
    html{
        scroll-behavior: smooth;
    }
</style>
<nav class="navbar navbar-expand-lg static-top">
    <div class="container">
    </div>
  </nav>
  <nav class="navbar navbar-expand-lg static-top">
  <div class="container">
        <div class="breadcrumb bg-white">
            <li class="breadcrumb-item"><a href="/user">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Pinjam Buku</li>
        </div>
  </div>
  </nav>
  <div class="container mt-3">
    <h2>Pinjam Buku</h2>
    <hr class="sidebar-divider d-none d-md-block">
    @if(session('danger'))
        <div class="alert alert-danger alert-dismissible fade show mb-2">
            {{ session('danger') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>

    @endif
  <!-- 2 column grid layout with text inputs for the first and last names -->
  <div class="row mb-4">
    <div class="col">
      <div class="form-outline">
        <label class="form-label" for="form6Example1">Nama</label>
        <input type="text" id="form6Example1" class="form-control" readonly value="{{$profiles->name}}"/>
      </div>
    </div>
    <div class="col"></div>
    <div class="col"></div>
    <div class="col"></div>
  </div>

  <!-- Text input -->
  <div class="form-outline mb-4">
    <label class="form-label" for="form6Example3">Alamat</label>
    <input type="text" id="form6Example3" class="form-control" readonly value="{{$profiles->address}}"/>
  </div>

    <form action="/user/book" method="POST">
    @csrf
        <div class="row mb-4">
            <div class="col">
            <div class="form-outline">
                <label class="form-label" for="form6Example1">Username</label>
                <input name="user_id" type="text" id="form6Example1" class="form-control bg-white" readonly value="{{ Auth::user()->username }}"/>
            </div>
            </div>
            <div class="col"></div>
            <div class="col"></div>
            <div class="col"></div>
        </div>
        <!-- Text input -->
        <div class="form-outline mb-4">
            <label class="form-label" for="form6Example4">Nama Buku</label>
            <input name="book_id" type="text" id="form6Example4" class="form-control" />
            <a href="#buku">Lihat buku</a>
        </div>

        <!-- Email input -->
        <div class="form-outline mb-4">
            <label class="form-label" for="form6Example5">Tanggal Meminjam</label>
            <input name="borrow_date" type="date" id="form6Example5" class="form-control" />
        </div>
        <!-- Email input -->
        <div class="form-outline mb-4">
            <label class="form-label" for="form6Example5">Tanggal Kembali</label>
            <input name="book_return" type="date" id="form6Example5" class="form-control" />
        </div>
        <!-- Submit button -->
        <button type="submit" class="btn btn-primary btn-block mb-4">Proses Peminjaman</button>
        </form>

    </div>
    <hr class="sidebar-divider d-none d-md-block">
    <h1 class="display-5" id="buku">Buku Yang tersedia</h1>
    <br>
    <br>
    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th>ID</th>
                <th>Title</th>
                <th>Author</th>
                <th>Publish</th>
            </tr>
        </thead>
        <tbody>
            @foreach($book as $b)
            <tr>
                <td>{{ $b->id}}</td>
                <td>{{ $b->title}}</td>
                <td>{{ $b->author}}</td>
                <td>{{ $b->publish}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
@endsection
