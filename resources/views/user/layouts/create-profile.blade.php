<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Create Profile</title>
    <!-- Custom fonts for this template-->
    <link href="{{ asset('/assets/vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">
    <!-- Custom styles for this template-->
    <link href="{{ asset('/assets/css/sb-admin-2.min.css') }}" rel="stylesheet">
    <style>
        @media only screen and (max-width: 955px) { 
            .tes{
                min-width: 400px;
            } 
        }
    </style>
</head>

<body >

    <div class="container ">

        <!-- Outer Row -->
        <div class="justify-content-center d-flex align-items-center">

            <div class="w-50 tes" style="margin-top:100px">

                <div class="card o-hidden border-0 ">
                    <div class="card-body ">
                        <!-- Nested Row within Card Body -->
                            <div class="">
                                <div class="p-5">
                                    <div class="text-center">
                                        <h1 class="h4 text-gray-900 mb-4">Create Your Profile!</h1>
                                    </div>
                                        <form action="/create-profile" class="user" method="POST"  >
                                        @csrf
                                            <div class="form-group">
                                                <input id="name" type="name" 
                                                    class="form-control form-control-user @error('name') is-invalid @enderror"
                                                    name="name" value="{{ old('name') }}" required autocomplete="name" autofocus
                                                    id="exampleInputname" aria-describedby="nameHelp"
                                                    placeholder="Enter name">
                                                
                                                @error('name')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>

                                            <div class="form-group">
                                                <input id="age" type="number" 
                                                    class="form-control form-control-user @error('age') is-invalid @enderror"
                                                    name="age" value="{{ old('age') }}" required autocomplete="age" autofocus
                                                    id="exampleInputage" aria-describedby="ageHelp"
                                                    placeholder="Enter Age...">
                                                
                                                @error('age')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>

                                            <div class="form-group">
                                                <input id="address" type="text" 
                                                    class="form-control form-control-user @error('address') is-invalid @enderror"
                                                    name="address" value="{{ old('address') }}" required autocomplete="address" autofocus
                                                    id="exampleInputaddress" aria-describedby="addressHelp"
                                                    placeholder="Enter address...">
                                                
                                                @error('address')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>

                                           


                                            <button type="submit" class="btn btn-primary btn-user btn-block">
                                                Create Profile
                                            </button>

                                        </form>
                                    <hr>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="{{ asset('/assets/vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('/assets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

    <!-- Core plugin JavaScript-->
    <script src="{{ asset('/assets/vendor/jquery-easing/jquery.easing.min.js') }}"></script>

    <!-- Custom scripts for all pages-->
    <script src="{{ asset('/assets/js/sb-admin-2.min.js') }}"></script>

</body>

</html>


