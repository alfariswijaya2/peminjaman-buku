@extends('user.template.master')

@section('content')
  <nav class="navbar navbar-expand-lg static-top">
    <div class="container">
    </div>
  </nav>
  <nav class="navbar navbar-expand-lg static-top">
  <div class="container">
        <div class="breadcrumb bg-white">
            <li class="breadcrumb-item"><a href="/user">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">History Peminjaman</li>
        </div>
  </div>
  </nav>
<hr class="sidebar-divider d-none d-md-block">
    <ul class="list-group list-group-flush">
        <li class="list-group-item"><b>Nama :</b> {{ $profiles->name }}</li>
        <li class="list-group-item"><b>Umur :</b> {{ $profiles->age }}</li>
        <li class="list-group-item"><b>Alamat :</b> {{ $profiles->address }}</li>
    </ul>

<br>
<h3 class="display-5" id="buku">History Peminjaman</h3>
<hr class="sidebar-divider d-none d-md-block">
<table class="table table-bordered"  width="100%" cellspacing="0">
    <thead>
        <tr>
            <th>Tanggal Peminjaman</th>
            <th>Tanggal Pengembalian</th>
            <th>Judul Buku</th>
            <th>Penulis</th>
            <th>Email</th>
        </tr>
    </thead>
    <tbody>
        @foreach($join as $b)
        <tr>
            <td>{{ $b->borrow_date}}</td>
            <td>{{ $b->book_return}}</td>
            <td>{{ $b->title}}</td>
            <td>{{ $b->author}}</td>
            <td>{{ $b->email}}</td>
        </tr>
        @endforeach
    </tbody>
</table>

@endsection