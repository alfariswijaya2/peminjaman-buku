@extends('user.template.master')

@section('content')
<style>
    .gambar{
        background-image: linear-gradient(rgba(0,0,0,0.5),rgba(0,0,0,0.5)),url("{{asset('/assets/img/perpus.jpg') }}");
        object-fit: cover;
        color: white;
    }
    .display-4{
        font-weight: 600;
    }
    .display-4{
        font-size: 50px; 
        font-weight: 600;
    }
</style>
  <nav class="navbar navbar-expand-lg static-top">
    <div class="container">
    </div>
  </nav>
  <nav class="navbar navbar-expand-lg static-top">
  <div class="container">
        <div class="breadcrumb bg-white">
            <li class="breadcrumb-item"><a href="/user">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Tridevbooks</li>
        </div>
  </div>
  </nav>
  <div class="jumbotron jumbotron-fluid gambar rounded">
  <div class="container ">
    <h1 class="display-4">Buku adalah Jendela dunia</h1>
    <p class="lead">Budayakan membaca buku, agar masa depanmu dapat menjadi tempat ilmu pengetahuan</p>
  </div>
</div>
<br>
<hr class="sidebar-divider d-none d-md-block">
<h1 class="display-5" id="buku">Buku Yang tersedia</h1>
<br>
<br>
<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
    <thead>
        <tr>
            <th>ID</th>
            <th>Title</th>
            <th>Author</th>
            <th>Publish</th>
        </tr>
    </thead>
    <tbody>
        @foreach($book as $b)
        <tr>
            <td>{{ $b->id}}</td>
            <td>{{ $b->title}}</td>
            <td>{{ $b->author}}</td>
            <td>{{ $b->publish}}</td>
        </tr>
        @endforeach
    </tbody>
</table>
<div class="row">
      <div class="col-lg-12 text-center">
        <h1 class="mt-5">Jika ingin mengakses halaman "/admin", ubah lah role anda menjadi Admin di Database tabel User</h1>
        <h1 class="mt-5">Halaman ini menggunakan  Bootstrap 4 Starter Template</h1>
        <p class="lead">Complete with pre-defined file paths and responsive navigation!</p>
        <ul class="list-unstyled">
          <li>Bootstrap 4.5.3</li>
          <li>jQuery 3.5.1</li>
        </ul>
      </div>
    </div>
@endsection