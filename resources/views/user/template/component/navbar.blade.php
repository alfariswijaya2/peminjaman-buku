<nav class="navbar navbar-expand-lg navbar-dark bg-dark static-top p-3">
    <div class="container">
      <a class="navbar-brand" href="/user"><i class="fas fa-book-open"></i>  Halo, {{ Auth::user()->username }}</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item ">
            <a class="nav-link" href="/user">Home
              <span class="sr-only">(current)</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/user/book">Peminjaman Buku</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/user/history"><i class="fas fa-bookmark"></i></a>
          </li>
          <li class="nav-item ">
            <a class="nav-link  p-2" href="{{ route('logout') }}"
                onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                <i class="fas fa-sign-out-alt text-danger"></i>
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
          </li>
        </ul>
      </div>
    </div>
  </nav>