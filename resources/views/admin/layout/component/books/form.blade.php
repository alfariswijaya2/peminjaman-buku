@extends('admin.template.master')

@section('content')

<div class = "ml-4 mt-4 mr-4">
<div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Form Buku</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="{{ route('books.store') }}" method ="POST">
              @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text" class="form-control" name="title" placeholder="Title">
                    @error('title')
                    <div class="alert alert-danger">
                    {{ $message }}
                    </div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="author">Author</label>
                    <input type="text" class="form-control" name="author" placeholder="Author">
                    @error('author')
                    <div class="alert alert-danger">
                    {{ $message }}
                    </div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="publish">Publish</label>
                    <input type="text" class="form-control" name="publish" placeholder="Publish">
                    @error('publish')
                    <div class="alert alert-danger">
                    {{ $message }}
                    </div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="genre">Genre</label>
                    <input type="text" class="form-control" name="genre" placeholder="romance, action, trhiller">
                    @error('genre')
                    <div class="alert alert-danger">
                    {{ $message }}
                    </div>
                    @enderror
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Create</button>
                </div>
              </form>
            </div>
</div>

@endsection