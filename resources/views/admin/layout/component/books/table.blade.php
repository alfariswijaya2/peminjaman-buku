@extends('admin.template.master')


@section('content')

<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
<h1 class="h3 mb-2 text-gray-800">Selamat Datang</h1>
<a href="/excel-book" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
<i class="fas fa-download fa-sm text-white-50"></i> Download Excel</a>
</div>
<p class="mb-4">Welcome to TriBooks's Book List</p>
<p><a target="_blank" href="https://datatables.net">Learn More</a></p>
                    <!-- DataTales Example -->
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">TriBooks's Book List</h6>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                        <a href="/admin/books/create" class="btn btn-info mb-2">Add Book</a>
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Title</th>
                                        <th>Author</th>
                                        <th>Publish</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($books as $b)
                                    <tr>
                                        <td>{{ $b->id}}</td>
                                        <td>{{ $b->title}}</td>
                                        <td>{{ $b->author}}</td>
                                        <td>{{ $b->publish}}</td>
                                        <td>
                                            <div class = "d-flex justify-content-around">
                                                <a href="/admin/books/{{ $b->id }}" class="btn btn-info">Show</a>
                                                <a href="/admin/books/{{ $b->id }}/edit" class="btn btn-primary">Edit</a>
                                                <form method="POST" action="/admin/books/{{ $b->id }}">
                                                    @csrf
                                                    @method('delete')
                                                    <input class="btn btn-danger" value="delete" type="submit">
                                                </form>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>


@endsection

@push('scripts')
<!--
<script>
    Swal.fire({
        title: "Data Berhasil Disimpan",
        text: "Terima Kasih Telah Mempercayai TriBooks",
        icon: "success",
        confirmButtonText: "Cilukba !!! linknya disini gan",
    });
    

    

    
</script>
-->
@endpush