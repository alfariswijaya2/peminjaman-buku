@extends('admin.template.master')


@section('content')

<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
<h1 class="h3 mb-2 text-gray-800">Selamat Datang</h1>
<a href="/excel-user" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
<i class="fas fa-download fa-sm text-white-50"></i> Download Excel</a>
</div>
<p class="mb-4">Welcome to TriBooks's Book List</p>
<p><a target="_blank" href="https://datatables.net">Learn More</a></p>
                    <!-- DataTales Example -->
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">TriBooks's Book List</h6>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Username</th>
                                        <th>E-Mail</th>
                                        <th>Role</th>
                                        <th>Action</th>
                                        
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($user as $u)
                                    <tr>
                                    <td>{{ $u->id}}</td>
                                        <td>{{ $u->username}}</td>
                                        <td>{{ $u->email}}</td>
                                        <td>{{ $u->role}}</td>
                                        <td>
                                            <div class = "d-flex justify-content-around">
                                                <a href="/admin/listusers/{{ $u->id }}" class="btn btn-info">Show</a>
                                                <a href="/admin/listusers/{{ $u->id }}/edit" class="btn btn-primary">Edit</a>
                                                <form method="POST" action="/admin/listusers/{{ $u->id }}">
                                                    @csrf
                                                    @method('delete')
                                                    <input class="btn btn-danger" value="delete" type="submit">
                                                </form>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>


@endsection